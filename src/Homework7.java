public class Homework7 {

    public static void main(String[] args) {

//        Write a simple application to finish the assignments below:
//
//        - Create a class that represents a banking account.
//        - Create a field that holds a balance (initially is 0)
//        - Create a method that deposits to the balance
//        - Create a method that withdraws from the balance
//        - Check that if a bank customer deposits $500, $150 and $35 and then withdraws $40 and $120 the balance is correct
//        - Print out a message if it is correct and vice versa
//
//        - Create a new branch and commit your changes
//        - Push your code to a remote repository
//        - Copy and paste the link of your branch in repository in the homework lesson text field, so we can check your homework
//        - Create a merge request

        BankingAccount myAccount = new BankingAccount();
        myAccount.deposit(500);
        // please finish the rest
    }
}

class BankingAccount {

    public int balance;

    public void deposit(int sum) {
        // do what?
    }

    // what about withdrawal?
}